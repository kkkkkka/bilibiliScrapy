import scrapy
from ..items import ScrapydemoItem
import json

class BlibliSpider(scrapy.Spider):
    name = 'blibli'
    #allowed_domains = ['https://search.bilibili.com/']
    num = 1

    def start_requests(self):
        for num in range(1,6):
            url = 'https://search.bilibili.com/all?keyword={}&from_source=webtop_search&spm_id_from=333.851&page={}'.format('熊本熊',num)
            yield scrapy.Request(url=url,callback=self.parse)

    def parse(self, response):
        all = response.xpath('//*[@class="video-list clearfix"]/li')
        item = ScrapydemoItem()
        list_data = []
        for i in all:
            item['name'] = i.xpath('./a[@class="img-anchor"]/@title').get()
            href = i.xpath('./a[@class="img-anchor"]/@href').get().split('//')[1]

            item['href'] = href

            href1 = href.split('/')[2].split("?")[0]
            table = 'fZodR9XQDSUm21yCkr6zBqiveYah8bt4xsWpHnJE7jL5VG3guMTKNPAwcF'
            tr = {}
            for n in range(58):
                tr[table[n]] = n
            s = [11, 10, 3, 8, 4, 6]
            xor = 177451812
            add = 8728348608

            r = 0
            for n in range(6):
                r += tr[href1[s[n]]] * 58 ** n

            data = (r - add) ^ xor

            list_data.append(data)
            yield item

        for i in list_data:
            for aa in range(1, 6):
                data_url = 'https://api.bilibili.com/x/v2/reply/main?jsonp=jsonp&next={}&type=1&oid={}'.format(aa, i)
                yield scrapy.Request(url=data_url, meta={'item': item}, callback=self.blibli_parse)

    def blibli_parse(self,response):
        item = response.meta['item']
        data = json.loads(response.text)["data"]['replies']
        if data is None:
            return '无数据'
        else:
            for i in data:
                data1 = i["content"]["message"]
                item['comment'] = str(data1)
                yield item
