from loguru import logger
from pathlib import Path
import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
# project_path = Path(__file__).parent
log_path = Path(BASE_DIR, "Log")
# 是否开启回溯
backtrace = True

rotation = "7 days"


def info_only(record):
    return record["level"].name == "INFO"


logger.add(log_path / "info_log.log", backtrace=backtrace, filter=info_only, rotation=rotation, encoding="utf-8",
           enqueue=True,
           retention="14 days")


def warning_only(record):
    return record["level"].name == "WARNING"


logger.add(log_path / "warning_log.log", backtrace=backtrace, filter=warning_only, rotation=rotation, encoding="utf-8",
           enqueue=True,
           retention="14 days")


def debug_only(record):
    return record["level"].name == "DEBUG"


logger.add(log_path / "debug_log.log", backtrace=backtrace, filter=debug_only, rotation=rotation, encoding="utf-8",
           enqueue=True,
           retention="3 days")


def error_only(record):
    return record["level"].name == "ERROR"


logger.add(log_path / "error_log.log", backtrace=backtrace, filter=error_only, rotation=rotation, encoding="utf-8",
           enqueue=True,
           retention="14 days")

if __name__ == '__main__':
    logger.info("中文test")
    logger.debug("中文test")
    logger.warning("中文test")
    logger.error("中文test")
