# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ScrapydemoItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    # 当当网采用下面的传输变量
    # name = scrapy.Field()  # 书名
    # author = scrapy.Field()  # 作者
    # introduction = scrapy.Field()  # 简介
    # price = scrapy.Field()  # 价格
    # press = scrapy.Field()  # 出版社

    # 哔哩哔哩搜索采用下面的传输变量
    # name = scrapy.Field() # 标题
    # href = scrapy.Field() # 链接
    # comment = scrapy.Field() #评论

    #哔哩哔哩热文排行榜采用下面传输变量
    hotID = scrapy.Field() #UP ID
    hotName = scrapy.Field() #UP名称
    hotTitle = scrapy.Field() #视频标题
    hotVideo = scrapy.Field() #视频播放数
    hotComment = scrapy.Field() #视频评论数
    hotUrl = scrapy.Field() #视频链接
    hotTime = scrapy.Field() #采集时间
    hotType = scrapy.Field() #视频类型

    #哔哩哔哩用户采集用下面传输变量
    userName = scrapy.Field()
    userMid = scrapy.Field()
    userFans = scrapy.Field()
    userFollower = scrapy.Field()
    userImage = scrapy.Field()
    userSex = scrapy.Field()
    userUrl = scrapy.Field()
