/*
SQLyog Community v13.1.8 (64 bit)
MySQL - 8.0.25 : Database - wjh_sql
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`wjh_sql` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `wjh_sql`;

/*Table structure for table `blibli_user` */

DROP TABLE IF EXISTS `blibli_user`;

CREATE TABLE `blibli_user` (
  `Mid` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'UP主ID',
  `Name` varchar(20) DEFAULT NULL COMMENT 'UP主名称',
  `Sex` varchar(20) DEFAULT NULL COMMENT 'UP主性别',
  `Image` varchar(200) DEFAULT NULL COMMENT 'UP主头像',
  `Fans` int DEFAULT NULL COMMENT 'UP主粉丝',
  `Follower` int DEFAULT NULL COMMENT 'UP主关注数',
  `Url` varchar(200) DEFAULT NULL COMMENT 'UP主空间URL',
  PRIMARY KEY (`Mid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

/*Table structure for table `bliblihotlist` */

DROP TABLE IF EXISTS `bliblihotlist`;

CREATE TABLE `bliblihotlist` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `hotTitle` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `hotID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '热文作者ID',
  `hotName` varchar(20) DEFAULT NULL COMMENT '热文作者名称',
  `hotUrl` varchar(1000) DEFAULT NULL COMMENT '热文视频链接',
  `hotVideo` int DEFAULT NULL COMMENT '热文视频播放数',
  `hotComment` int DEFAULT NULL COMMENT '热文视频评论数',
  `hotType` varchar(20) DEFAULT NULL COMMENT '热文视频分区',
  `hotTime` date DEFAULT NULL COMMENT '采集时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=226839 DEFAULT CHARSET=utf8mb3;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
